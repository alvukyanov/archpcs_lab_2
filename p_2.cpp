
#include <openmpi/mpi.h>
#include <vector>

#include <fstream>
#include <iostream>
double startT, stopT;

std::vector<int> mergeArrays(std::vector<int> v1, std::vector<int> v2)
{
    int i, j, k;
    //int* result;
    std::vector<int> result(v1.size() + v2.size());
    //result = new int[n1 + n2];
    i = 0;
    j = 0;
    k = 0;

    while (i < v1.size() && j < v2.size())
        if (v1[i] < v2[j]) {
            result[k] = v1[i];
            i++;
            k++;
        } else {
            result[k] = v2[j];
            j++;
            k++;
        }

    if (i == v1.size())
        while (j < v2.size()) {
            result[k] = v2[j];
            j++;
            k++;
        }
    if(j == v2.size())
        while (i < v1.size()) {
            result[k] = v1[i];
            i++;
            k++;
        }

    return result;
}
/*
int* mergeArrays (int* v1, int n1, int* v2, int n2)
{
    int i, j, k;
    int* result;

    result = new int[n1 + n2];
    i = 0;
    j = 0;
    k = 0;

    while (i < n1 && j < n2)
        if (v1[i] < v2[j]) {
            result[k] = v1[i];
            i++;
            k++;
        } else {
            result[k] = v2[j];
            j++;
            k++;
        }

    if (i == n1)
        while (j < n2) {
            result[k] = v2[j];
            j++;
            k++;
        }
    if(j == n2)
        while (i < n1) {
            result[k] = v1[i];
            i++;
            k++;
        }

    return result;
}*/

void sort (std::vector<int> v)
{
    unsigned long i, j;
    for (i = v.size() - 2; i >= 0; i--)
        for (j = 0; j <= i; j++)
            if (v[j] > v[j + 1])
                std::swap(v[j], v[j + 1]);
}



int main (int argc, char ** argv)
{
    //int* data;            //Our unsorted array
    std::vector<int> unsorted_array, sorter_array;
    //int* result_array; //Sorted Array
    //int* sub;

    int m, n;
    int id, p;
    int r;
    int s;
    int i;
    int move;
    MPI_Status status;

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &id);
    std::cout << "Rank: " << id << '\n';
    MPI_Comm_size (MPI_COMM_WORLD, &p);
    std::cout << "Size: " << p << '\n';

    //Task Of The Master Processor
    if (id == 0) {
        n = 100; //count of elements
        s = n / p;
        r = n % p;
        //data = new int[n + s - r];
        unsorted_array.resize(n + s - r);

        srand(unsigned (int(MPI_Wtime())));
        std::cout << "Reading data from input file...\n";
        std::ofstream file("input");

        for(i = 0; i < n; i++)
        {
            //data[i] = rand() % 15000;
            unsorted_array[i] = rand() % 15000;
            file << unsorted_array[i] << " ";
            std::cout << unsorted_array[i] << " ";
        }
        std::cout << '\n';
        file.close();

        if (r != 0) {
            for (i = n; i < n + s - r; i++)
            {
                //data[i] = 0;
                unsorted_array[i] = 0;
            }
            s = s + 1;
        }
        std::cout << "Parallel variant starting...\n";
        startT = MPI_Wtime();                                  //Start The Time.
        MPI_Bcast (&s, 1, MPI_INT, 0, MPI_COMM_WORLD);
        sorter_array.resize(s);
        //result_array = new int[s]; //Allocating result array
        //Sending data array from master to all other slaves
        MPI_Scatter (&unsorted_array, s, MPI_INT, &sorter_array, s, MPI_INT, 0, MPI_COMM_WORLD);
        sort(sorter_array);
    } else {
        MPI_Bcast (&s, 1, MPI_INT, 0, MPI_COMM_WORLD);
        //Allocating resultant array
        //result_array = new int[s];
        sorter_array.resize(s);
        MPI_Scatter (&unsorted_array, s, MPI_INT, &sorter_array, s, MPI_INT, 0, MPI_COMM_WORLD);
        //Each slave processor will sort according to the array partitioning n/p
        sort(sorter_array); //Sort the array up to index s.
    }

    move = 1;

    //Merging the sub sorted arrays to obtain resultant sorted array
    while (move < p) {
        if (id % (2 * move) == 0) {
            if (id + move < p) {                     //Receive
                MPI_Recv (&m, 1, MPI_INT, id + move, 0, MPI_COMM_WORLD, &status);
                std::vector<int> sub(m);
                //sub = new int [m]; //Allocate space for sub array
                MPI_Recv (&sub, m, MPI_INT, id + move, 0, MPI_COMM_WORLD, &status);
                //Obtaing resultant array by merging sub sorted array
                //result_array = mergeArrays (result_array, s, sub, m);
                sorter_array = mergeArrays(sorter_array, sub);
                s = s + m;
            }
        } else { //Send datas to neighbour processors
            int near = id - move;
            MPI_Send (&s, 1, MPI_INT, near, 0, MPI_COMM_WORLD);
            //MPI_Send (result_array, s, MPI_INT, near, 0, MPI_COMM_WORLD);
            break;
        }

        move = move * 2;
    }

    //Final Part, In this part Master CPU outputs the results.!!!
    if (id == 0) {
        stopT = MPI_Wtime();
        double parallelTime = stopT - startT;
        std::cout << "Parallel result: \n";
        for(i = 0; i < n; i++)
        {
            std::cout << sorter_array[i] << " ";
        }
        std::cout << "\n\n\nParallel time: " << parallelTime << '\n';

        std::cout << "Consistently variant starting...\n";
        startT = MPI_Wtime();
        sort(unsorted_array);
        stopT = MPI_Wtime();

        double poslTime = stopT - startT;
        std::cout << "Consistently result: \n";
        for(i = 0; i < n; i++)
        {
            std::cout << unsorted_array[i] << " ";
        }

        std::cout << "\n\n\nConsistently time: " << poslTime << '\n';
        std::cout << "SpeedUp:  \n\n\n" << poslTime/parallelTime << '\n';

        std::ofstream file("output");
        for(i = 0; i < n; i++)
        {
            file << sorter_array[i] << " ";
        }

        file.close();
    }

    MPI_Finalize (); //Finalize MPI environment.

    return 0;
}